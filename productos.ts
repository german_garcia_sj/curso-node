// productos.ts

import { isEmptyBindingElement, isEmptyStatement } from "typescript";

// Creando una lista de productos
interface Producto { 
    id: number;
    nombre: string; 
    precio: number;
    stock: number; 
  } 
  //
  const Producto1: Producto = { 
    id: 0,
    nombre: "Yogurt", 
    precio: 100,
    stock: 13
  }; 
  const Producto2: Producto = { 
   id: 1,
   nombre: "Paquete de Galletas", 
   precio: 89.9,
   stock: 40
 };
 const Producto3: Producto = { 
   id: 2,
   nombre: "Lata Coca-Cola", 
   precio: 80.50,
   stock: 0
 };
 const Producto4: Producto = {
   id: 3,
   nombre: "Alfajor",
   stock: 12,
   precio: 40
 }
 
 
 let productos:Array<Producto> = [
   Producto1,
   Producto2,
   Producto3,
   Producto4
 ]
 //obj producto
 export function objaux (i: number){
   return productos[i];
 }

 //GET
 export function getStock(){
   return productos  
 }
  //GET ID
  export function getStockId(idp:number){
    for (let i = 0; i < productos.length; i++) {
      if( productos[i].id === idp){
         return true;      
      }
    }
    return false; 
  }

//POST
 export function storeProductos(body:any){
   const q = productos.length
   productos.push({
    id: q, 
    nombre: body.nombre,
    precio: body.precio,
    stock: body.stock    
   })
   return productos[q]
 }

 //PUT
 export function updateProductos(body:any,id:number){
   const AuxProductos: Producto = {
   id: id,
   nombre: body.nombre,
   precio: body.precio,
   stock: body.stock    
  }
  for (let i = 0; i < productos.length; i++) {
    if( productos[i].id === AuxProductos.id){
       if (AuxProductos.nombre ===''){
          AuxProductos.nombre = productos[i].nombre
       }
       if (AuxProductos.stock ===null){
        AuxProductos.stock = productos[i].stock
       }
       if (AuxProductos.precio ===null){
        AuxProductos.precio = productos[i].precio
       }
       productos[i] = AuxProductos
      return true;//productos[i];      
    }
  }
  return false;//('Producto no existe'); 
}
//DELETE
export function deleteProductos(id:number){
  let idAux =null
  for (let i = 0; i < productos.length; i++) {
    if( productos[i].id === id){
      idAux = id     
    }
  }

  if (idAux !== null){
    productos.splice(idAux,1) //splice borra elemento de lista 
    console.log(productos.length)
    for (let i = 0; i < productos.length; i++) {
      productos[i].id = i
    }
    return true;
  }
  else return false;  
}

 /*function getStock (productos:Array<Producto>) {
 
   for (let i = 0; i < productos.length; i++) {
     if( productos[i].stock === 0 ){
       console.log(`Lo sentimos, no hay mas unidades del Producto ${productos[i].nombre}`);      
     }else{
       console.log(`Del Producto ${productos[i].nombre} quedan ${productos[i].stock} unidades`);    
     }    
   }
 }
 getStock(productos)
 */

