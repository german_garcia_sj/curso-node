// Importamos el paquete express
import express, { request } from 'express'
import { convertCompilerOptionsFromJson } from 'typescript';

import * as productos from './productos.js'

// Instanciamos una app de express
const app = express()
app.use(express.json()); //metodo de express para reconocer obj JSON 

// Definimos una ruta y su handler correspondiente
app.get('/', function (request, response) {
    response.send('¡Bienvenidos a Express!')
})

// Ponemos a escuchar nuestra app de express
app.listen(3000, function () {
    console.info('Servidor escuchando en http://localhost:3000')
});
// GET COMPLETO
app.get('/productos',function(request, response) {
    response.send(productos.getStock())    
})
//GET POR ID
app.get('/productos/id/:id',function(request, response) {
    //PARA PARAMETRO ID VACIO DA ERROR (No lo pude solucionar)
   let id1 = request.params.id
    id1 = id1.replace(/\s/g,'') //Elimina cualquier espacio en blanco dentro del parámetro
    if (id1 !== '') {        
        const id = Number(id1)
        const busqueda = productos.getStockId(id)
        if (busqueda===true){   
            response.send(productos.objaux(id))
        }
        else{
            response.status(400).json('Producto no existe')   
        }
    }
    else  response.status(400).json('Id Vacío con espacios en blanco - Corregir')
    
})
//PUSH
app.post('/productos/nuevo', function(request, response) { 
    let body = request.body
    if (body.nombre === '' || body.stock === null || body.precio ===null){
        response.status(400).json('No pueden haber campos vacíos')
    }
    else response.send(productos.storeProductos(body))    
})
//PUT
app.put('/productos/update/:id', function(request, response) {
    //PARA PARAMETRO ID VACIO DA ERROR (No lo pude solucionar)
    const body = request.body
    let id1 = request.params.id
    id1 = id1.replace(/\s/g,'') //Elimina cualquier espacio en blanco dentro del parámetro
    if (id1 !== '') {        
        const id = Number(id1)
        const busqueda =productos.updateProductos(body,id)
        if (busqueda===true){  
            response.send(productos.objaux(id))
        }
        else{
            response.status(400).json('Producto no existe')   
        } 

    }
    else  response.status(400).json('Id Vacío con espacios en blanco - Corregir')
})

app.delete('/productos/borrar/:id', function(request, response) {
    //PARA PARAMETRO ID VACIO DA ERROR (No lo pude solucionar)
    let id1 = request.params.id
    id1 = id1.replace(/\s/g,'') //Elimina cualquier espacio en blanco dentro del parámetro
    if (id1 !== '') {        
        const id = Number(id1)
        const busqueda = productos.deleteProductos(id)
        if (busqueda===true){ 
            response.send('Producto Borrado')
        }
        else{
            response.status(400).json('Producto no existe')   
        }
    }
    else  response.status(400).json('Id Vacío con espacios en blanco - Corregir')
})
/*
res.status(200) // Ok
res.status(201) // Created
res.status(204) // No content
res.status(400) // Bad request
res.status(401) // Unauthorized
res.status(403) // Forbidden
res.status(404) // Not found
res.status(500) // Server error
*/